#!/bin/bash
[% c("var/set_default_env") -%]
[% pc(c('var/compiler'), 'var/setup', {
        compiler_tarfile => c('input_files_by_name/' _ c('var/compiler')),
        gradle_tarfile => c("input_files_by_name/gradle"),
    }) %]
distdir=/var/tmp/dist
mkdir -p /var/tmp/build
mkdir -p $distdir/[% project %]

tar -C /var/tmp/dist -xf [% c('input_files_by_name/rust') %]
tar -C /var/tmp/dist -xf [% c('input_files_by_name/cbindgen') %]
tar -C /var/tmp/dist -xf [% c('input_files_by_name/nasm') %]
tar -C /var/tmp/dist -xf [% c('input_files_by_name/node') %]
tar -C /var/tmp/dist -xf [% c('input_files_by_name/clang') %]
export LLVM_CONFIG="/var/tmp/dist/clang/bin/llvm-config"
tar -C /var/tmp/dist -xf [% c('input_files_by_name/compiler-rt') %]
cp -r /var/tmp/dist/compiler-rt/* /var/tmp/dist/clang/
tar -C /var/tmp/dist -xf [% c('input_files_by_name/binutils') %]
export PATH="/var/tmp/dist/rust/bin:/var/tmp/dist/cbindgen:/var/tmp/dist/nasm/bin:/var/tmp/dist/node/bin:/var/tmp/dist/clang/bin:/var/tmp/dist/binutils/bin:$PATH"

[% IF c("var/rlbox") -%]
    tar -C /var/tmp/dist -xf [% c('input_files_by_name/wasi-sysroot') %]
    # XXX: We need the libclang_rt.builtins-wasm32.a in our clang lib directory.
    # Copy it over.
    # https://searchfox.org/mozilla-central/source/build/build-clang/build-clang.py#890,
    # include it directly in our clang
    rtdir=/var/tmp/dist/clang/lib/clang/[% pc("clang", "version") %]/lib/wasi
    mkdir -p $rtdir
    cp /var/tmp/dist/wasi-sysroot/lib/clang/*/lib/wasi/libclang_rt.builtins-wasm32.a $rtdir
    export WASI_SYSROOT=/var/tmp/dist/wasi-sysroot/share/wasi-sysroot
[% END -%]

tar -C /var/tmp/build -xf [% project %]-[% c('version') %].tar.gz

cd /var/tmp/build/[% project %]-[% c("version") %]
cat > .mozconfig << 'MOZCONFIG_EOF'
. $topsrcdir/mozconfig-[% c("var/osname") %]

export MOZ_INCLUDE_SOURCE_INFO=1
export MOZ_SOURCE_REPO="[% c('var/gitlab_project') %]"
export MOZ_SOURCE_CHANGESET=[% c("var/git_commit") %]
MOZCONFIG_EOF

[% c("var/set_MOZ_BUILD_DATE") %]

export JAVA_HOME=/usr/lib/jvm/java-1.11.0-openjdk-amd64
gradle_repo=/var/tmp/dist/gradle-dependencies
export GRADLE_MAVEN_REPOSITORIES="file://$gradle_repo","file://$gradle_repo/maven2"
export GRADLE_FLAGS="--no-daemon --offline"
# Move the Gradle repo to a hard-coded location. The location is embedded in
# the file chrome/toolkit/content/global/buildconfig.html so it needs to be
# standardized for reproducibility.
mv $rootdir/[% c('input_files_by_name/gradle-dependencies') %] $gradle_repo
cp -r $gradle_repo/m2/* $gradle_repo

rm -f configure
rm -f js/src/configure

# We unbreak mach, see: https://bugzilla.mozilla.org/show_bug.cgi?id=1656993 and https://bugzilla.mozilla.org/show_bug.cgi?id=1755516
export MACH_BUILD_PYTHON_NATIVE_PACKAGE_SOURCE=system
# Create .mozbuild to avoid interactive prompt in configure
mkdir "$HOME/.mozbuild"
./mach configure \
    --with-base-browser-version=[% c("var/torbrowser_version") %] \
    [% IF !c("var/rlbox") -%]--without-wasm-sandboxed-libraries[% END %]

./mach build --verbose

# We don't want the debug or "exoplayer" .aars, but the .aar that has `omni` in its name.
find obj-* -type f -name geckoview*omni*.aar -exec cp {} $distdir/[% project %] \;

cd $distdir
[% c('tar', {
        tar_src => [ project ],
        tar_args => '-czf ' _ dest_dir _ '/' _ c('filename'),
    }) %]
